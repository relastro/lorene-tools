#!/usr/bin/env python
'''
This script looks through the directories and produces a file that lists
the enthalpy, baryon masses, and gravitational masses from Lorene output.
'''
import os
import re
import argparse

parser = argparse.ArgumentParser(description='Produces table of enthalphy, baryon and gravitional masses \
from a LORENE binary neutron star simulation.')

parser.add_argument('-d', dest='dkw', help='the directory keyword that contains all the BNS data. Default is "bns".' ,
   default="bns")
parser.add_argument('--out',dest='out',help='the name of the output file to save all the data. Default is "ent_bary_grav.txt".',
   default="ent_bary_grav.txt")
parser.add_argument('--in',dest='input',help='the name of the input file that contains the output of the LORENE initialisation.  Default is "init_bin.out".', 
   default="init_bin.out")

args = parser.parse_args()

##settings 
dirKeyword=args.dkw
initOutfile=args.input
outputFile=args.out

def get_masses(inFile,searchStr):
  #function based on Filippo's script
  f=open(inFile,'r')
  flt=re.compile(searchStr)
  #apply regex to all the lines in the output file
  b_masses=filter(flt.match,f.readlines())
  #the last two lines are always the baryon mass of star 1 and start 2
  #and in those lines the 2nd to last entry is always the mass
  mstar1=b_masses[-2].split()[-2]
  mstar2=b_masses[-1].split()[-2]
  f.close()
  return float(mstar1),float(mstar2)
 
#get all bns directories
dirs=os.listdir('.')
#get only directories that have the keyword in them
bns_dirs=[x for x in dirs if dirKeyword in x]
bns_dirs.sort()

f=open(outputFile,'w')
f.write("# enthalphy  | mb 1 | mb 2 | mg 1 | mg 2 | r1 | r2 | c1 | c2\n")
for x in bns_dirs: 
  fileString=str(x)+'/'+str(initOutfile)
  ent=float(x.split('_')[1])
  mb1,mb2=get_masses(fileString,'Baryon mass')
  mg1,mg2=get_masses(fileString,'Gravitational mass')
  r2,r1=get_masses(fileString,'Coordinate polar radius a3')
  #compute compactness C = M/R  (radius is given in km)
  c1=mg1/(r1/1.4769994423016508)
  c2=mg2/(r2/1.4769994423016508)
  #output as enthalpy | bar 1 | bar 2 | grav 1 | grav 2 | r1 | r2  | c1  | c2
  print '{0:.3f} {1:.9f} {2:.9f} {3:.9f} {4:.9f} {5:.9f} {6:.9f} from {7}'.format(ent,mb1,mb2,mg1,mg2,r1,r2,fileString)
  f.write('{0:.3f} {1:.9f} {2:.9f} {3:.9f} {4:.9f} {5:.9f} {6:.9f} {7:.9f} {8:.9f}\n'.format(ent,mb1,mb2,mg1,mg2,r1,r2,c1,c2))

f.close()
