#!/usr/bin/env python
# Script to update the paramter file for coal to put in the correct masses from init_bin
import os
import shutil
import glob
import numpy as np
import math 
import argparse

parser = argparse.ArgumentParser(description='Modifies the parcoal.d files with the baryon masses from initial condition simulations.')

parser.add_argument('-d', dest='dkw', help='the directory keyword that contains all the BNS data. Default is "bns".' , default="bns")
parser.add_argument('-f', dest='dfile', help='the file that contains all the mass, enthlaphy, and radius data. Default is "ent_bary_grav.txt".', default="ent_bary_grav.txt")
parser.add_argument('--thresh',dest='thresh', help='The convergence threshold for stopping the simulation. Default is "1.0E-7".', default="1.0E-7")
args = parser.parse_args()

dirKeyword=args.dkw 
data=np.loadtxt(args.dfile)
thresh=args.thresh

dirs=os.listdir('.')
#get only directories that have the keyword in them
bns_dirs=[x for x in dirs if dirKeyword in x]
bns_dirs.sort()

k=0
for x in data[:,0]:
  x="{0:.3f}".format(x)
  l=dirKeyword+"_"+str(x) 
  print l
  os.chdir(l)

  #Read and modify the parameter file 
  paramFile = open('parcoal.d','r')
  paramLines = paramFile.readlines()
  paramLines[4] = str(data[k,1]) +"\tmbar_voulue[0] : Baryon mass required for star 1 [M_sol]\n"
  paramLines[5] = str(data[k,2]) +"\tmbar_voulue[1] : Baryon mass required for star 2 [M_sol]\n"
  paramLines[12] = str(thresh) +"\tseuil : Threshold on the enthalpy relative change for ending the computation\n" 
  paramFile.close()
  paramFile = open('parcoal.d','w')
  paramFile.writelines(paramLines)
  paramFile.close()
  os.chdir('..')
  k+=1
 
