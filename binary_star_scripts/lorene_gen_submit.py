#!/usr/bin/env python
'''
This script generates a submit script for the given directory
'''
import os
import argparse
import math

parser = argparse.ArgumentParser(description='Generates a submit scripts for LORENE initial data.')

parser.add_argument('-d', dest='dkw', help='the directory keyword that contains all the BNS data. Default is "bns".' , default="bns")
parser.add_argument('--exe', dest='exe', help='The name of the executable.',required=True)
parser.add_argument('--error', dest='error', help='The name of the error files. Default is "lorene".',default="lorene")
parser.add_argument('--out',dest='out',help='The name of the output file for LORENE to dump data.', required=True)
parser.add_argument('--sname',dest='sname',help='The name of the job script. Default is "submit_script.sh" ', default="submit_script.sh")
parser.add_argument('--time',dest='time',help='the time of the simulation in HH:MM:SS.', default="5:00:00")

args = parser.parse_args()

 
dirKeyword=args.dkw
execName=args.exe
outName=args.out
errorOut=args.error
scriptName=args.sname
time=args.time
#get all bns directories
dirs=os.listdir('.')
#get only directories that have the keyword in them
bns_dirs=[x for x in dirs if dirKeyword in x]
bns_dirs.sort()
nnodes=int(math.ceil(len(bns_dirs)/24.))
ntasks=nnodes*24
cwd=os.getcwd()

print args
print "There are {0} directories. Changed nodes and ntasks appropriately.".format(len(bns_dirs))
print "Make sure these values are sensible."
print "nodes = {0} \nntasks = {1}".format(nnodes,ntasks)
print "Make sure to change the time to the correct value!"
print "time = {0}".format(time)

#now write all the data to a file
f=file(scriptName,'w')

f.write("#!/bin/bash\n\
#SBATCH --partition=parallel\n\
#SBATCH --ntasks={0}\n\
#SBATCH --constraint=dual\n\
#SBATCH --nodes={1}\n\
#SBATCH --cpus-per-task=1\n\
#SBATCH --mem-per-cpu=2000\n\
#SBATCH --time={2}\n\
#SBATCH --error={3}\n\
#SBATCH --output={4}\n\
export OMP_NUM_THREADS=1\n\
module purge\n\
module load deprecated\n\
module load slurm/2.6.3\n\
module load intel/compiler/64/13.0/2013.2.146\n\
module load gcc/4.8.2\n".format(ntasks,nnodes,time,cwd+"/"+errorOut+".err",cwd+"/"+errorOut+".out")
)


for x in bns_dirs:
  f.write("cd " + str(cwd) + "/" + str(x) + "/ && ./" + execName + " > " + outName + " 2>&1 &\n")
  
f.write("# Wait for all child processes to terminate.\nwait\n")

f.close()

print "Wrote data to : {0}".format(scriptName)
