#!/usr/bin/env python
#Script to generate TOV sequences using realistic EOS and Lorene spectral solver
# it does this by copying a directory Bin_star which contains the executables and parameters
# and copies it to multiple directories with different enthalpy values
import os
import shutil
import glob
import numpy as np
import math 
import argparse

parser = argparse.ArgumentParser(description='Sets up a LORENE ID directory by creating folders with different enthalphies')

parser.add_argument('-d', dest='dkw', help='the directory keyword that contains all the BNS data. Default is "bns".' , default="bns")
parser.add_argument('--se', dest='se', help='The start enthalphy.',required=True,type=float)
parser.add_argument('--ee', dest='ee', help='The end  enthalphy.',required=True,type=float)
parser.add_argument('--diff', dest='diff', help='The difference between enthalphy values',type=float,default=0.001)
#parser.add_argument('--error', dest='error', help='The name of the error files. Default is "lorene".',default="lorene")
#parser.add_argument('--out',dest='out',help='The name of the output file for LORENE to dump data.', required=True)
#parser.add_argument('--sname',dest='sname',help='The name of the job script. Default is "submit_script.sh" ', default="submit_script.sh")
#parser.add_argument('--time',dest='time',help='the time of the simulation in HH:MM:SS.', default="5:00:00")

args = parser.parse_args()
#Fix the central enthalpy of star 1
startEnthalpy = args.se #0.18
endEnthalpy  = args.ee #0.226
dirKeyword=args.dkw
stride=args.diff

Nval=math.floor((endEnthalpy-startEnthalpy)/stride)+1

temp_dir=[]
for Index in np.linspace(startEnthalpy,endEnthalpy,Nval):
  #enthalpy for the current folder
  newEnthalpy=str(Index)
  temp_dir.append(str(dirKeyword)+"_"+newEnthalpy.ljust(5,"0"))

print "The directories created will be : " , temp_dir

print "IF YOU WANT TO MAKE CHANGES ENTER N OTHERWISE PRESS ENTER"
ans=raw_input()
if ans=='N':
  quit()


#loop over enthalpy changes and copy data to folder
for Index in np.linspace(startEnthalpy,endEnthalpy,Nval):
  #enthalpy for the current folder
  newEnthalpy=str(Index)
  new_dir = 'bns_'+newEnthalpy.ljust(5,"0")
  os.makedirs(new_dir)
  source_dir = 'Bin_star'
  
  print "Creating directory: " + new_dir
  #copy the parameter files of the Bin_star directory to each new directory
  for filename in glob.glob(os.path.join(source_dir, '*.d')):
    shutil.copy(filename, new_dir)   
  #copy the executables to the new directory
  shutil.copy2('Bin_star/init_bin',new_dir)
  shutil.copy2('Bin_star/coal',new_dir)
  
  
  os.chdir(new_dir)

  #Read and modify the parameter file 
  paramFile = open('par_init.d','r')
  paramLines = paramFile.readlines()
  paramLines[4] = newEnthalpy + "\t<- initial central enthalpy of star 1\n"
  paramLines[6] = newEnthalpy + "\t<- initial central enthalpy of star 2\n"
  paramFile.close()
  paramFile = open('par_init.d','w')
  paramFile.writelines(paramLines)
  paramFile.close()
  os.chdir('..')
 
