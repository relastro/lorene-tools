1	=1 if graphical outputs, =0 otherwise
2	1= binary black hole, 2= binary neutron star
resu.d
33	nr_a : number of points in r for the centered grid
25	nt_a : number of points in theta for the centered grid
24	np_a : number of points in phi for the centered grid
20.	rayon_a [Lorene unit = 10 km] : inner radius of the last domain
