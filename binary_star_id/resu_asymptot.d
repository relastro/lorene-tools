nom de fichier :resu.d
nr_a : number of points in r for the centered grid = 33
nt_a : number of points in theta for the center grid = 25
np_a : number of points in phi for the centered grid = 24
rayon_a (10 km) : inner radius of the last domain = 20


Asymptotic behavior of N

Spectral coefficients of N (1/r^0) : 
nn0_max =1
k= 0 j= 0 : 1

Spectral coefficients of N (1/r^1) : 
nn1_max =0.383071
k= 0 j= 0 : -0.383071

Spectral coefficients of N (1/r^2) : 
nn2_max =0.0733713
k= 0 j= 0 : 0.0733713

Spectral coefficients of N (1/r^3) : 
nn3_max =2.59852
k= 0 j= 0 : 0.852139
k= 0 j= 1 : 2.59852
k= 4 j= 0 : -2.58789
k= 4 j= 1 : 2.58817
