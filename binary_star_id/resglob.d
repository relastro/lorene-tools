0    step
0    abscidia of the rotation axis [km] 
1374.148277610574    Orbital frequency Omega [rad/s] 
0.02242340684255328    Abscidia ``center of mass'' star 1 [km] 
0.02242340684255328    Abscidia ``center of mass'' star 2 [km] 
1.559315023520914   Baryon mass of star 1  [M_sol] 
0.006976066332053837   relative variation enth. star 1
10.59883587709264   R(theta=0) [km] 
10.73485076651797   R(theta=pi/2, phi=0) [km] 
10.51441083693219   R(theta=pi/2, phi=pi/2) [km] 
10.55988833032871   R(theta=pi/2, phi=pi) [km] 
1.559315023520914   Baryon mass of star 2  [M_sol] 
0.006976066332053837   relative variation enth. star 2
10.59883587709264   R(theta=0) [km] 
10.73485076651797   R(theta=pi/2, phi=0) [km] 
10.51441083693219   R(theta=pi/2, phi=pi/2) [km] 
10.55988833032871   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
1    step
0    abscidia of the rotation axis [km] 
1441.982770809634    Orbital frequency Omega [rad/s] 
0.06863919345787117    Abscidia ``center of mass'' star 1 [km] 
0.06863919345787117    Abscidia ``center of mass'' star 2 [km] 
1.42374322760089   Baryon mass of star 1  [M_sol] 
0.002359497730986569   relative variation enth. star 1
10.26345501229811   R(theta=0) [km] 
10.35206573762132   R(theta=pi/2, phi=0) [km] 
10.19615413400569   R(theta=pi/2, phi=pi/2) [km] 
10.32428516163644   R(theta=pi/2, phi=pi) [km] 
1.42374322760089   Baryon mass of star 2  [M_sol] 
0.002359497730986569   relative variation enth. star 2
10.26345501229811   R(theta=0) [km] 
10.35206573762132   R(theta=pi/2, phi=0) [km] 
10.19615413400569   R(theta=pi/2, phi=pi/2) [km] 
10.32428516163644   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
2    step
0    abscidia of the rotation axis [km] 
1460.059550062402    Orbital frequency Omega [rad/s] 
0.02945357049431774    Abscidia ``center of mass'' star 1 [km] 
0.02945357049431774    Abscidia ``center of mass'' star 2 [km] 
1.429476154885217   Baryon mass of star 1  [M_sol] 
0.0009499649285521286   relative variation enth. star 1
10.26152848528114   R(theta=0) [km] 
10.3270063292648   R(theta=pi/2, phi=0) [km] 
10.21173213260616   R(theta=pi/2, phi=pi/2) [km] 
10.41693285082692   R(theta=pi/2, phi=pi) [km] 
1.429476154885217   Baryon mass of star 2  [M_sol] 
0.0009499649285521286   relative variation enth. star 2
10.26152848528114   R(theta=0) [km] 
10.3270063292648   R(theta=pi/2, phi=0) [km] 
10.21173213260616   R(theta=pi/2, phi=pi/2) [km] 
10.41693285082692   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
3    step
0    abscidia of the rotation axis [km] 
1413.325931899227    Orbital frequency Omega [rad/s] 
-0.0009765845712417232    Abscidia ``center of mass'' star 1 [km] 
-0.0009765845712417232    Abscidia ``center of mass'' star 2 [km] 
1.42187218834474   Baryon mass of star 1  [M_sol] 
0.001188477256917726   relative variation enth. star 1
10.26505943632904   R(theta=0) [km] 
10.40778089875019   R(theta=pi/2, phi=0) [km] 
10.22461596236492   R(theta=pi/2, phi=pi/2) [km] 
10.44121729488289   R(theta=pi/2, phi=pi) [km] 
1.42187218834474   Baryon mass of star 2  [M_sol] 
0.001188477256917726   relative variation enth. star 2
10.26505943632904   R(theta=0) [km] 
10.40778089875019   R(theta=pi/2, phi=0) [km] 
10.22461596236492   R(theta=pi/2, phi=pi/2) [km] 
10.44121729488289   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
4    step
0    abscidia of the rotation axis [km] 
1394.113766620909    Orbital frequency Omega [rad/s] 
0.01054627556293486    Abscidia ``center of mass'' star 1 [km] 
0.01054627556293486    Abscidia ``center of mass'' star 2 [km] 
1.443666491434594   Baryon mass of star 1  [M_sol] 
0.001015256495225619   relative variation enth. star 1
10.30830603278398   R(theta=0) [km] 
10.48812700492966   R(theta=pi/2, phi=0) [km] 
10.27453659218788   R(theta=pi/2, phi=pi/2) [km] 
10.51183898662349   R(theta=pi/2, phi=pi) [km] 
1.443666491434594   Baryon mass of star 2  [M_sol] 
0.001015256495225619   relative variation enth. star 2
10.30830603278398   R(theta=0) [km] 
10.48812700492966   R(theta=pi/2, phi=0) [km] 
10.27453659218788   R(theta=pi/2, phi=pi/2) [km] 
10.51183898662349   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
5    step
0    abscidia of the rotation axis [km] 
1381.871589274617    Orbital frequency Omega [rad/s] 
0.01027284602354239    Abscidia ``center of mass'' star 1 [km] 
0.01027284602354239    Abscidia ``center of mass'' star 2 [km] 
1.45214594147105   Baryon mass of star 1  [M_sol] 
0.0009015159069633554   relative variation enth. star 1
10.3306904380442   R(theta=0) [km] 
10.50329981263417   R(theta=pi/2, phi=0) [km] 
10.29653484144171   R(theta=pi/2, phi=pi/2) [km] 
10.54412675818999   R(theta=pi/2, phi=pi) [km] 
1.45214594147105   Baryon mass of star 2  [M_sol] 
0.0009015159069633554   relative variation enth. star 2
10.3306904380442   R(theta=0) [km] 
10.50329981263417   R(theta=pi/2, phi=0) [km] 
10.29653484144171   R(theta=pi/2, phi=pi/2) [km] 
10.54412675818999   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
6    step
0    abscidia of the rotation axis [km] 
1465.319762810616    Orbital frequency Omega [rad/s] 
0.006269090383588249    Abscidia ``center of mass'' star 1 [km] 
0.006269090383588249    Abscidia ``center of mass'' star 2 [km] 
1.494860518185962   Baryon mass of star 1  [M_sol] 
0.002217279839383541   relative variation enth. star 1
10.30628955717048   R(theta=0) [km] 
10.55334235213388   R(theta=pi/2, phi=0) [km] 
10.25888400067681   R(theta=pi/2, phi=pi/2) [km] 
10.4571648902626   R(theta=pi/2, phi=pi) [km] 
1.494860518185962   Baryon mass of star 2  [M_sol] 
0.002217279839383541   relative variation enth. star 2
10.30628955717048   R(theta=0) [km] 
10.55334235213388   R(theta=pi/2, phi=0) [km] 
10.25888400067681   R(theta=pi/2, phi=pi/2) [km] 
10.4571648902626   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
7    step
0    abscidia of the rotation axis [km] 
1505.362008806882    Orbital frequency Omega [rad/s] 
0.04705761715762424    Abscidia ``center of mass'' star 1 [km] 
0.04705761715762424    Abscidia ``center of mass'' star 2 [km] 
1.464287855973612   Baryon mass of star 1  [M_sol] 
0.0006893447212162689   relative variation enth. star 1
10.23525316610172   R(theta=0) [km] 
10.45070143946523   R(theta=pi/2, phi=0) [km] 
10.18171448449879   R(theta=pi/2, phi=pi/2) [km] 
10.42423482672763   R(theta=pi/2, phi=pi) [km] 
1.464287855973612   Baryon mass of star 2  [M_sol] 
0.0006893447212162689   relative variation enth. star 2
10.23525316610172   R(theta=0) [km] 
10.45070143946523   R(theta=pi/2, phi=0) [km] 
10.18171448449879   R(theta=pi/2, phi=pi/2) [km] 
10.42423482672763   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
8    step
0    abscidia of the rotation axis [km] 
1532.426078016303    Orbital frequency Omega [rad/s] 
0.02729613040129752    Abscidia ``center of mass'' star 1 [km] 
0.02729613040129752    Abscidia ``center of mass'' star 2 [km] 
1.448681187827934   Baryon mass of star 1  [M_sol] 
0.0004749801634844482   relative variation enth. star 1
10.19572372407082   R(theta=0) [km] 
10.3741212895544   R(theta=pi/2, phi=0) [km] 
10.14356114690844   R(theta=pi/2, phi=pi/2) [km] 
10.42712008698589   R(theta=pi/2, phi=pi) [km] 
1.448681187827934   Baryon mass of star 2  [M_sol] 
0.0004749801634844482   relative variation enth. star 2
10.19572372407082   R(theta=0) [km] 
10.3741212895544   R(theta=pi/2, phi=0) [km] 
10.14356114690844   R(theta=pi/2, phi=pi/2) [km] 
10.42712008698589   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
9    step
