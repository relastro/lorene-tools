#!/bin/bash
#
# Based on ~/inital-data/Magstars-data/create-magstar.sh.
#
# Lorene Magstar frontend script by Sven Koeppel, 2015
#
# This is a small script to handle the recompilation, run, parameters
# and exporter of the Lorene magstar code.
# --> and also creates runscripts with individual runs, etc.
#
# -- Sven Koeppel, 2015 CC0
#

# quiet pushd/popd
pushd () { command pushd "$@" > /dev/null; }
popd ()  { command popd "$@" > /dev/null;  }

# set up Lorene environment
source ~/initial-data/lorene-env.sh

echo "$0: Called with arguments '$@'"

# the paths to some binaries
read_mag_ns="$LORENE_HOME/Export/MagNS/read_mag_ns"
magstar="$LORENE_HOME/Codes/Magstar/magstar"

exit_usage() {
	echo -e "Usage: $0 [option] param-and-output-directory"
	echo -e "	-c	recompiles"
	echo -e "	-r	read resu.d"
	echo -e "	-m	Run the magstar code: Create the star"
	echo -e "	-n      create a new param and output directory"
	echo -e "	-p	setup ET parameter file and create ET simulation"
	echo -e "	-s	Submit ET simulation (implies -p if needed)"
	echo -e ""
	echo -e "  Typical examples:  $0 -cm simulation-name"
	echo -e "                     $0 -ms simulation-name"
	echo -e ""
	exit 1
}

[[ $# -eq 2 ]] || exit_usage

while getopts "crmnps" opt; do
    case $opt in 
        c ) recompile=1;;
	n ) new=1;;
	r ) read=1;;
	m ) run_magstar=1;;
	p ) setup_et=1;;
	s ) submit_et=1;;
        * ) exit_usage;;
    esac
done
shift $((OPTIND-1))

# to switch between the number of remaining arguments
case $# in
    1 ) working_dir="$(basename $1)";;
    2 ) exit_usage
	# or see this example
	infile=$1
        outfile=$2;;
    * ) exit_usage;;
esac

# to loop over the arguments:
#for working_dir in "$@"; do
#working_dir="$(basename "$working_dir")"
#echo "Handling $working_dir"

# exit on errors
set -e

if [[ $new ]];
	then echo "Creating the new working directory $working_dir"
	mkdir -p "$working_dir"
	echo "Copying the basis parameter files"
	cp -av template/* "$working_dir"

	exit 0
fi

if [[ $recompile ]];
	then echo "Recompiling Lorene..."

	echo "Making Export/C++"
	pushd "$LORENE_HOME/Export/C++"
	make
	popd

	echo "Making Export/MagNS"
	pushd "$LORENE_HOME/Export/MagNS"
	make uninstall && make
	popd
fi

if [[ $read ]];
	then echo "Just reading in the resu.d data in $working_dir"
	cd "$working_dir"

	exec > >(tee "read.log")
	exec 2>&1

	echo "Calling read_mag_ns at $(hostname) at $(date) in $(pwd)"

	$read_mag_ns
	exit $?
fi

# ET: submission needs setup before.
if [[ $submit_et && ! -f "$working_dir/$working_dir.par" ]]; then
	setup_et=1	
fi

if [[ $setup_et ]];
	then echo "Setting up the parameter file for $working_dir"

	# rename the ET parameter file
	parfile="$working_dir/$working_dir.par"
	[[ -f "$parfile" ]] || mv "$working_dir/template_ET.par" "$parfile"

	# replace the template variable
	realpath="$(readlink -f "$working_dir")"
	sed -i "s#RESU_D_PLACEHOLD_ME#$realpath#" "$parfile"

	# go to ET and create simulation
	pushd /home/astro/koeppel/ET/lorene-mns-thc/Cactus
	simfactory/bin/sim create "$working_dir" --machine loewe \
		--parfile "$realpath/$working_dir.par" --configuration thcMNS3 \
	&& echo "Done setting up simulation" \
	|| { echo "Error setting up simulation"; exit 1; }

	popd
fi

if [[ $run_magstar ]]; then
	# Run the star creation, log the output
	exec > >(tee "$working_dir/run.log")
	exec 2>&1

	echo "This is $0 running on $(hostname) at $(date)"
	echo "Using Lorene at $LORENE_HOME"
	echo "Using working directory '$working_dir'"
	cd "$working_dir"

	# create the magnet star
	{ LC_ALL="C" LANG="C" time $magstar; } || {
		echo "Error creating the magstar"; exit 1;
	}

	echo "Finished creating the magstar. Results are in 'resu.d'"
fi

if [[ $submit_et ]];
	then echo "Submitting job $working_dir"

	pushd /home/astro/koeppel/ET/lorene-mns-thc/Cactus
	simfactory/bin/sim submit "$working_dir" --machine loewe \
		--walltime 24:00:00 --procs 96
fi
