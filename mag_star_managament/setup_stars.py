#!/usr/bin/env python

# This script is supposed to live in the root of your star
# series directories.

## -- Sven, 2016 

# needs Python 2.7
# use python for powerful argument parsing capabilities
import argparse, sys

# Needs Numpy
from numpy import linspace

# builtins
from itertools import product
from os import path
from distutils.dir_util import copy_tree
import fileinput
import re
from collections import namedtuple


interactive = __name__ == "__main__"
parser = argparse.ArgumentParser(description='Setup individual or a bunch of magstar initial data directories')

def range_parser(arg):
	values = [float(x) for x in arg.split('-')]
	if len(values) == 1:
		return values[0]
	if len(values) != 2:
		raise ValueError("Three values given: %s. Only two allowed" % str(values))
	return values

def construct_range(value, value_count):
	return linspace(start=value[0], stop=value[1], num=value_count) if type(value) != float else [value]

enth_group = parser.add_argument_group('Star enthalpy', 'Give an enthalpy (range). Enthalpy is given in [c^2] units (Lorene standard)')
enth_group.add_argument('--enth', metavar='<range/float>', dest='enth', type=range_parser, help='Min-Max enthalpy', default=0.225)
enth_group.add_argument('--enth-cnt', metavar='num', dest='enth_count', type=int, help='Number of enthalpy points incl. endpoint', default=10)

freq_group = parser.add_argument_group('Rotation Frequency', 'Give an frequency (range). Frequency is given in [hz] units (Lorene standard)')
freq_group.add_argument('--freq', metavar='<range/float>', dest='freq', type=range_parser, help='Min-Max frequency', default=0.)
freq_group.add_argument('--freq-cnt', metavar='num', dest='freq_count', type=int, help='Number of frequency points incl. endpoint', default=10)

parser.add_argument('-r', '--run', metavar='dirname', dest='run_name', type=str, help='Name of run (must exist, with template directory)', default='')

if interactive:
	args = parser.parse_args()

	# post-composing the arguments
	enth = construct_range(args.enth, args.enth_count)
	freq = construct_range(args.freq, args.freq_count)
	prefix = args.run_name+'/' if args.run_name else ''

	template_dir = prefix + "template/"
	if not path.isdir(template_dir):
		parser.error("Did not found star parameter template directory '%s'" % template_dir)

# Starting the program


class Star:
	def __init__(self, enth,freq,prefix,template_dir):
		self.enth, self.freq, self.prefix, self.template_dir = enth, freq, prefix, template_dir

		self.dirname = '%sstar-enth%f-freq%f/' % (self.prefix, self.enth, self.freq)
		Line = namedtuple('Param', 'line value')
		self.data = [
			# Lines counting from 1.
			Line(3, "%f\tent_c : Central enthalpy in c^2 set by setup-stars.py" % self.enth),
			Line(4, "%f\tfreq_si : Rotation frequency in Hz set by setup-stars.py" % self.freq)
		]

	def __str__(self):
		return self.dirname
	def exists(self):
		return path.isdir(self.dirname)
	def setup(self):
		# copy all files from template dir
		copy_tree(self.template_dir, self.dirname, preserve_symlinks=1)

		# change the parameters
		parrot = path.join(self.dirname, 'parrot.d')
		f = fileinput.input(parrot, inplace=1)
		#datalines = max([x.line for x in self.data])
		for line in f:
			curline = f.lineno()
			line = line.rstrip() # chop the newline
			for datum in self.data:
				if curline == datum.line:
					print datum.value
					break
			else:
				print line

if interactive:
	combinations = product(enth, freq)
	stars = [Star(enth,freq,prefix,template_dir) for enth,freq in combinations]
	
	print "Creating these %d stars: " % len(stars)
	for star in stars: print star
	
	# TODO: CHECK WITH ANY ob verzeichnis schon existiert!
	# TODO: FRAGEN YES NO
	
	print "Now creating %d stars" % len(stars)
	
	for i, star in enumerate(stars):
		print "[%d/%d] %s" % (i, len(stars), str(star))
		star.setup()
	



