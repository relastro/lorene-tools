#!/bin/bash
#
# LOAD THIS SCRIPT INTO THE CURRENT SHELL AS "source ../lorene-env.sh"
#
# Sven Readme:
# Lorene auf Loewe installiert am 28. April 2015.
#
# Luke Readme:
# Lorene on Loewe installed on 01. January 2016. 
#  - modified the script a little bit
#
# Mit folgenden Modulen:
#
# Currently Loaded Modulefiles:

# this is from Filipos lorene_modules list, this should be updated to latest version at some point

module purge
module load deprecated 
module load slurm/2.6.3
module load intel/compiler/64/13.0/2013.2.146
module load gcc/4.8.2

# this is a list of mine
module load fftw3/gcc/64/3.3.4
module load lapack/gcc/64/3.3.0

# set LORENE_HOME directory. We cannot access $0 when
# sourcing this script, so this won't work:
#   export LORENE_HOME="$(dirname "$0")/Lorene"

#this points to where the main Lorene directory is
LOR="/scratch/astro/bovard/initial-data/Lorene"
#this points to where the pgplot installation is
PGPLOT="/scratch/astro/bovard/initial-data/lib/pgplot"

echo $LOR
echo "Setting lorene home to $LOR"
export LORENE_HOME="$LOR"
export HOME_LORENE="$LOR"
unset LOR

# setting up pgplot
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${PGPLOT}"
export PGPLOT_DIR="${PGPLOT}"

# noninteractive output device: Coulored postscript, see also
# http://www.atnf.csiro.au/computing/software/miriad/userguide/node25.html
# by default, file is called "pgplot.ps".
export PGPLOT_DEV="/CPS"
