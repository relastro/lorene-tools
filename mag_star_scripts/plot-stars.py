#!/usr/bin/python

from pylab import *
ion()

data = genfromtxt("./run3-table.txt", skip_header=1, names=True)

# group data by frequency column
freqs = unique(data[:]['freq'])
data_by_freq = [data[data[:]['freq']==f] for f in freqs]

# plot each data
for D in data_by_freq:
	f = D[0]['freq']
	plot(D['enth'], D['massG'], "o-")

legend()
