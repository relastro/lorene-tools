#!/usr/bin/env python
#
# List stars by reading logfiles after magstar crashed.
# Magstar crashes if no solution is found or when searching for the
# Keplerian frequency, which means increasing the frequency more and more.
#
# Run this script not with calcul.d (not even present) but with the directory
# name. Will then look for the files parrot.d and frequency.d
#
# Usage eg. out of this directory:
#   python keplerian-list-star.py ~/initial-data/ns-stability-diag/run6/star-*/ > ~/initial-data/ns-stability-diag/run6/table.txt
#
# ATTENTION: This script is the only one in the set of log-list-star.py et al
#     which is not working with resu.d or any file, but directories. This also
#     applies for the output table (dirname column instead of fname column)!
#
# -- SvenK 2016-12-10
#

from sys import argv, exit
from os.path import isfile, isdir, join
from re import search, split

print "# Magstar Keplerian crash extractor of %d stars" % (len(argv)-1)

FORMATSTRING = "%3s\t%15s\t%15s\t%15s\t%15s\t%15s\t%s"
KEYS = ("num", "enth", "freq", "max_step", "initial_massB", "initial_massG", "dirname")

print FORMATSTRING % KEYS

# helper function to get a column in a line in a file
def col(star_dname, fname, line, col=0):
	fcontent = list(open(join(star_dname, fname), "r"))
	return split("\s+", fcontent[line])[col]

# helper function to search for a specific string
def findf(star_dname, fname, regexp):
	with open(join(star_dname, fname), 'r') as f:
		for line in f:
			m = search(regexp, line)
			if m:
				return float(m.group(1))
	return -1 # nothing found

FIRST=0 # first python list element index
SECOND=1 # second python list element index
LAST=-1 # last python list element index

findings = {
	'enth': lambda star: col(star, "parrot.d", line=2, col=FIRST),
	'freq':  lambda star: col(star, "frequency.d", line=LAST, col=SECOND),
	# these values are with f=0, before frequency turned on. Likely bad.
	'initial_massB': lambda star: findf(star, "run.log", r"^Baryon mass :\s+([\d.]+)"),
	'initial_massG': lambda star: findf(star, "run.log", r"^Gravitational mass :\s+([\d.]+)"),
	'max_step':  lambda star: col(star, "frequency.d", line=LAST, col=FIRST),
}

for i, dname in enumerate(argv[1:]):
	if not isdir(dname):
		print "Error: Directory '%s' not readable" % dname
		exit(1)

	#num, enth, ... default values = -1
	res = { k: -1 for k in findings.keys() }

	res['num'] = i
	res['dirname'] = dname

	for key, func in findings.iteritems():
		res[key] = func(dname)

	print FORMATSTRING % tuple([ res[k] for k in KEYS])

