#!/bin/sh

# as always, this environment variable does not work at all.
export OPENMP_NUM_THREADS="40"

# this is neccessary
. ~/initial-data/lorene-env.sh

exec ./list-stars $@
