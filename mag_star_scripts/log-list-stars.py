#!/usr/bin/env python
#
# List stars by reading their logfile.
# Do not call this with "resu.d" as arguments, but
# with "calcul.d" files.
#
# Usage eg. out of this directory:
# python log-list-stars.py  ~/initial-data/ns-stability-diag/run3/star-*/calcul.d > ~/initial-data/ns-stability-diag/run3/table.txt
#

from sys import argv, exit
from os.path import isfile
from collections import namedtuple
from re import search, sub

print "# Magstar resu.d regexp extractor of %d stars" % (len(argv)-1)


findings = {
	'1_enth': r"^Central enthalpy :\s+([\d.]+)",
	'2_ener': r"^Central proper energy density :\s+([\d.]+)",
	'3_freq': r"f :\s+([\d.]+) Hz\s*$",
	'4_massB': r"^Baryon mass :\s+([\d.]+)",
	'5_massG': r"^Gravitational mass :\s+([\d.]+)",
	'6_r_circ': r"^Coordinate equatorial radius \(phi=0\) a1 =\s+([\d.]+)",
	'7_axes_ratio': r"^Flattening r_pole/r_eq :\s+([\d.]+)",
	'8_J': r"^Angular momentum J :\s+([\d.]+)"
}

# format string generation
fsprop = namedtuple('fsprop', ['width', 'specifier'])
inum, ifilename = '0_num', '99_filename'
headline_prop = { inum: fsprop('3', 's'), ifilename: fsprop('', 's') }
data_prop = headline_prop.copy()
headline_prop.update({ k: fsprop('15', 's') for k in findings.keys() })
data_prop.update(    { k: fsprop('15', 'e') for k in findings.keys() })
columns = sorted(headline_prop.keys())
cleancolumns = [ sub(r'^\d+_(.+)', '\\1', c) for c in columns ] # poor mans SortedDict
formatstring = lambda dataprop: "\t".join(['%'+dataprop[c].width+dataprop[c].specifier for c in columns])
headline = formatstring(headline_prop)
dataline = formatstring(data_prop)

print headline % tuple(cleancolumns)

# print headline should do this:
#print "%3s\t%15s\t%15s\t%15s\t%15s\t%15s\t%15s\t%s" % \
#	("num", "enth", "ener", "freq", "massB", "massG", "r_circ", "filename")

for i, fname in enumerate(argv[1:]):
	if not isfile(fname):
		print "Error: File '%s' not readable" % fname
		exit(1)

	#num, enth, ener, freq, massB, massG, r_circ = -1,-1,-1,-1,-1,-1,-1
	res = { k: -1 for k in findings.keys() }

	res[inum] = i
	res[ifilename] = fname

	with open(fname, 'r') as f:
		for line in f:
			for key, regexp in findings.iteritems():
				m = search(regexp, line)
				if m:
					res[key] = float(m.group(1))

	print dataline % tuple([ res[k] for k in columns ]) 

	# static columns version:
	#print "%3d\t%15e\t%15e\t%15e\t%15e\t%15e\t%15e\t%s" % \
	#	tuple([ res[k] for k in ["num", "enth", "ener", "freq", "massB", "massG", "r_circ", "filename"]])

