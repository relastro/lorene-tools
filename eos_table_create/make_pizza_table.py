''' 
This script computes the PIZZA ID file from the 
raw table from stellarcollapse.org

Potential improvements: 
 - interpolation of the quantities at beta equilibrium?
 - make pizza not shit
'''
import matplotlib.pyplot as plt
import numpy as np
import h5py
import argparse

parser = argparse.ArgumentParser(description='Create a LORENE ID file in the eos_akmalpr.d format')

parser.add_argument('-t','--table',dest='table',help='The hdf5 table from stellarcollapse.org')
parser.add_argument('-o','--output',dest='outfile',help="The name of the output file to save the table.", default="table.lorene")
parser.add_argument('--shift',help='The table has an energy shift. ',default=False,action='store_true')

## GET MBAR FROM TABLE!?!?!?!
mbar=1.675e-24 #mass of the neutron in g
cm3_to_fm3=1.0e39 # 1 cm^3 in fm^3 
c_cgs = 29979245800.  # Vacuum speed of light
c2=c_cgs*c_cgs


if __name__=='__main__':
  options = parser.parse_args()

  hasEnergyShift=options.shift
  rawTableFile=str(options.table)
  outputFile=str(options.outfile)

  rawTable=h5py.File(rawTableFile,"r")

  #get the data needed. units from stellarcollapse reader pdf
  table_log_rho=np.array(rawTable['logrho']) # log  g/cm^3
  table_log_pressure=np.array(rawTable['logpress']) #log dyne / cm^2
  table_log_energy=np.array(rawTable['logenergy']) # log erg / g
  table_log_temp=np.array(rawTable['logtemp']) # log MeV
  table_cs2=np.array(rawTable['cs2']) # log MeV
  munu=np.array(rawTable['munu'])
  table_ye=np.array(rawTable['ye'])

  print "Generating PIZZA ID with  T = {:} MeV".format(10**table_log_temp[0])
  if hasEnergyShift==True:
    energyShift=np.array(rawTable['energy_shift'])[0] # erg/g

  #exponentiate log quantities
  pressure=10**table_log_pressure[:,0,:] #table is stored as ye x T x rho  
  eps=10**table_log_energy[:,0,:] #table is stored as ye x T x rho 
  rho=10**table_log_rho #get rho
  T=10**table_log_temp
  

  
  nplogrho=np.array(rawTable['pointsrho'])[0] #the number of points in rho
  npye=np.array(rawTable['pointsye'])[0] #the number of points in rho
  

  ye_beta=[]
  ye_list=[]
  #find beta equilibrium point, i.e. where munu=0
  # we assume that munu is monotonic and then just
  # take the first positive value. 
  for k in range(0,nplogrho):
    for j in range(0,npye):
      if (munu[j,0,k]>0):
        ye_list.append(table_ye[j])
        break
    ye_beta.append(j)
    
  #compute the quantities needed in LORENE units
  pnew,enew,cs2new=[],[],[]
  for k in range(0,nplogrho):
    p=pressure[ye_beta[k],k]
    e=eps[ye_beta[k],k]
    cs2=table_cs2[ye_beta[k],0,k]
    pnew.append(p)
    enew.append(e)
    cs2new.append(cs2)
  
  pnew=np.array(pnew)
  enew=np.array(enew)
  cs2new=np.array(cs2new)
  if hasEnergyShift==True:
    enew=enew-energyShift 

  #find the point where cs^2 > 1
  #otherwise pizzaidbase complains about superluminal speeds
  if np.where(cs2new/c2>1)[0].shape>0:
    print "Superluminal speeds"
    idx=np.where(cs2new/c2>1)[0][0] 
    idx=cs2new.shape
  else:
    idx=cs2new.shape
  
  idx=nplogrho-1
  # density = [kg/m^3]
  pizza_rho = rho[0:idx]*1000.

  # specific internal energy [ dimensionless]
  pizza_sed = enew[0:idx]/c2

  # pressure  [ kg / m s^2] 
  pizza_p =  0.1*pnew[0:idx]

  # enthalpy [dimensionless]
  pizza_h = 1+pizza_sed + pizza_p/pizza_rho/c2

  # cs^2 [dimesionless]
  pizza_cs2 = cs2new[0:idx]/c2/pizza_h
  pizza_cs2 = cs2new[0:idx]*pizza_rho / (c2*(pizza_rho+pizza_rho*pizza_sed)+pizza_p) 


  # g ???
  pizza_gm1 = cs2new[0:idx]/c2

  #T [Mev]
  pizza_T = T[0]*np.ones(nplogrho)[0:idx]

  #ye [dimensionless]
  pizza_ye = ye_list[0:idx]


  #header that is needed for LORENE. Formatting is important.
  saveFile=outputFile
  headerString='name = {:}\ntype = tabulated\nisentropic = 1\nhas_temp = 1\nhas_efrac = 1'.format(rawTableFile) 
  np.savetxt(saveFile,np.column_stack((pizza_rho,pizza_sed,pizza_p,pizza_cs2,pizza_gm1,pizza_T,pizza_ye,pizza_h)),header=headerString)
  print "Warning! You must remove the # before the top five lines for it work in pizza"

  

