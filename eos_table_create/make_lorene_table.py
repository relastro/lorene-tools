''' 
This script computes the LORENE ID file from the 
raw table from stellarcollapse.org

Potential improvements: 
 - interpolation of the quantities at beta equilibrium?
'''
import matplotlib.pyplot as plt
import numpy as np
import h5py
import argparse

parser = argparse.ArgumentParser(description='Create a LORENE ID file in the eos_akmalpr.d format')

parser.add_argument('-t','--table',dest='table',help='The hdf5 table from stellarcollapse.org')
parser.add_argument('-o','--output',dest='outfile',help="The name of the output file to save the table.", default="table.lorene")
parser.add_argument('--shift',help='The table has an energy shift. ',default=False,action='store_true')

## GET MBAR FROM TABLE!?!?!?!
mbar=1.675e-24 #mass of the neutron in g
cm3_to_fm3=1.0e39 # 1 cm^3 in fm^3 
c_cgs = 29979245800.  # Vacuum speed of light
c2=c_cgs*c_cgs


if __name__=='__main__':
  options = parser.parse_args()

  hasEnergyShift=options.shift
  rawTableFile=str(options.table)
  outputFile=str(options.outfile)

  rawTable=h5py.File(rawTableFile,"r")

  #get the data needed. units from stellarcollapse reader pdf
  table_log_rho=np.array(rawTable['logrho']) # log  g/cm^3
  table_log_pressure=np.array(rawTable['logpress']) #log dyne / cm^2
  table_log_energy=np.array(rawTable['logenergy']) # log erg / g
  table_log_temp=np.array(rawTable['logtemp']) # log MeV
  munu=np.array(rawTable['munu'])

  print "Generating Lorene ID with  T = {:} MeV".format(10**table_log_temp[0])
  if hasEnergyShift==True:
    energyShift=np.array(rawTable['energy_shift'])[0] # erg/g

  #exponentiate log quantities
  pressure=10**table_log_pressure[:,0,:] #table is stored as ye x T x rho  
  eps=10**table_log_energy[:,0,:] #table is stored as ye x T x rho 
  rho=10**table_log_rho #get rho

  
  nplogrho=np.array(rawTable['pointsrho'])[0] #the number of points in rho
  npye=np.array(rawTable['pointsye'])[0] #the number of points in rho
  

  ye_beta=[]
  #find beta equilibrium point, i.e. where munu=0
  # we assume that munu is monotonic and then just
  # take the first positive value. 
  for k in range(0,nplogrho):
    for j in range(0,npye):
      if (munu[j,0,k]>0):
        break
    ye_beta.append(j)
    
  #compute the quantities needed in LORENE units
  pnew,enew=[],[]
  for k in range(0,nplogrho):
    p=pressure[ye_beta[k],k]
    e=eps[ye_beta[k],k]
    pnew.append(p)
    enew.append(e)
  
  pnew=np.array(pnew)
  enew=np.array(enew)
  if hasEnergyShift==True:
    enew=enew-energyShift 
  # baryon number density = rho/mbar [fm^{-3}]
  lorene_bnd=rho/mbar/cm3_to_fm3 

  # pressure is unchanged [dyne /cm^2]
  lorene_pressure = pnew

  # total energy density  e = rho(1+eps/c^2) [g/cm^3]
  lorene_ted = rho*(1+enew/c2)

  #header that is needed for LORENE. Formatting is important.
  headerString='computed at beta equilibrium from {1}\nNote idx does nothing. Needed for lorene format\n\n\n\n{0}\n\n\n idx | baryon number density [fm^-3] | energy density [g/cm^3] | pressure [dyne/cm^2]'.format(nplogrho,rawTableFile) 
  index=42*np.ones(nplogrho) #index needed for LORENE. Value is unimportant
  saveFile=outputFile
  np.savetxt(saveFile,np.column_stack((index,lorene_bnd,lorene_ted,lorene_pressure)),header=headerString,fmt='%d %1.16e %1.16e %1.16e')
  print "Warning! You must remove the # before the number in the created file for it work in LORENE."

  

