In this little tutorial, I will discuss how to create initial data in Lorene.

To create binary star initial data, you must use the code provided in $HOME_LORENE/Lorene/codes/Bin_star

Unfortunately the way the data is produced is rather poorly designed and things must be done in a very specific way.

In the above folder there will be numerous *.C files that contain the various important code to create the initial data. Futhermore there is 
also files that in *.d which is sometimes a binary format and sometimes a plain text file. Very annoying. 

The two important files that we want are:

init_bin.C
coal.C

The first file contains information about setting up the initial conditions for the initial data
The second file computes the actual equilibrium configuration of the binary system.

The input files for init_bin.C are:
	par_init.d
	par_grid1.d
	par_grid2.d 
	par_eos1.d
	par_eos2.d

The output files for init_bin.C are:
	ini.d

The input files for coal.C are:
	parcoal.d

The output files for coal.C are:
	resu.d 


Rather unfortunately *.d are both binary and text files which doesn't make much sense. Additionally, the order of parameters in the 
parameter files is fixed! This means that if you switch the positions of two lines, this will cause the code to break. Don't do this.


++++++++++++++++
OUTPUT FILES
++++++++++++++++

ini.d

If you happen to stumble across this file and have no idea what initial data it represents, and there is nothing else there,
you are probably out of luck. Unless you want to read the binary files which is hard.  

resu.d

This is the final initial data that is required by the einstein tookit to run. This is a very important file. Again it is a binary
file so if you don't have any associated data, you are screwed.


+++++++++++
INPUT FILES 
+++++++++++

par_init.d - this file contains very general parameters such as rotation / irrotation

par_grid#.d - these files contain the grid paramters. I don't touch these.

par_eos#.d - these files contain the EOS parameters. Lorene has numerous EOS ids (see eos.h) and this file selects those.

parcoal.d - this file contains the initial parameters to find the equilibrium configuration

+++++++++++++++
CREATING BNS ID
+++++++++++++++
1. First compile the code to generate a ./coal and ./init_bin executables.

2. Modify the par_init.d file and change the enthalpy. There are other parameters to play with here, but I never touch them. 
  Most variables are descriptive in explaining what they do. To change the mass of the stars, simply change the enthalpy.
  Do not modify the distance between them. If you do want to move the stars, this is done at a later step. Simply keep the coordinate
	distance fixed.

3. Execute init_bin. This can be run somewhere because it normally takes less then 5 minutes to run.
   The init_bin executable solves the TOV equations to find an equilibrium configuration of a single NS for both stars. It treats the spacetime
   as if there is nothing else there except the star. 

4. Obtain the gravitational mass and baryon mass from the output of init_bin. The file should have a line like this

   Baryon mass :        1.357366284470603 M_sol
 	 Gravitational mass : 1.252416316426517 M_sol

 	These numbers are very important. In this case, we will be creating BNS initial data with a gravitational mass of 1.25 M. This means that each star
  if infinitely separated from each other, would have a gravitational mass of 1.25 M. This is also quoted as M_\inf. The baryon mass is what we need 
  for the next step. 

5. In parcoal.d we must now modify the the fields that require the baryon mass. For example, taking the above data, we would have something like

	# Parameters for the binary equilibrium computation by coal
	###################################################################
	ini.d
	0.45    fact_separ : Factor by which the initial separation is multiplied
	1.357366284     mbar_voulue[0] : Baryon mass required for star 1 [M_sol]
	1.357366284     mbar_voulue[1] : Baryon mass required for star 2 [M_sol]
	###################################################################
	500     mermax : Maximum number of steps in the main iteration 
	0.55    relax :  Relaxation factor in the main iteration 
	1       mermax_eqb : Maximum number of steps in Etoile_bin::equilibrium 
	1       prompt : 1 if no pause during the computation
	0       graph : 1 if graphical outputs during the computation 
	1.0E-7  seuil : Threshold on the enthalpy relative change for ending the computation

  REmember the order is fixed so you cannot modify the ordering of the parameters. Here mbar_voulue[0/1] are set to the baryon mass. The fact_sept tells us that
  of an initial separation of 100 km, we want 0.45 or 45 km. A funny way but this is how it works. 

  Finally seuil is an important quantity. This sets the convergence criteria. Here I am using 1.0e-7. This is a standard value and should be good enough for your work.
 
6. Run coal and wait. Initial data can take about 1 day to produce with a convergence of 1E-7. You can monitor this convergence by looking at the file "resconv1.d" which 
  plots this threshold versus iteration. This is important to look at to ensure that the solution is indeed converging. Also take a look at the actual coal.C to see the
  exact details since that is also very straightforward.

7. Assuming that you have converged, you should now have a file called "resu.d" which will contain the initial data that is required. COPY EVEYRTHING IN THE FOLDER to wherever
  you store ID for your simulation and modify the parfile accordingly.



+++++++++++++++
CHANGING EOS
+++++++++++++++
Place the eos file in the parent folder and write the following in par_eos*, where "ALF2_pp_Lorene.d" is the name of your table.

17      Type of the EOS
0       0: standard format      1: CompOSE format
Tabulated EoS
../ALF2_pp_Lorene.d

To create the actual table is a different task, but it needs to be put into the LORENE format. There are other scripts to do that. At some point, I will describe them, but they should be
called EOSTOOLS or something. Also on Loewe we have plenty of already computed EOS in /home/astro/shared/projekt/EOS_Collection



++++++++++++
MANAGING ID
++++++++++++
I wrote some scripts in ~/binary_star_scripts which should help in doing the above ID production steps. Checkout the readmes there to understand what is done. 
